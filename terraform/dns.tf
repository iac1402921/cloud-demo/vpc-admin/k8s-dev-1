resource "dns_a_record_set" "k8s-dev-bq-dns" {
  zone = var.dns_zone
  name = "bq.demo"
  addresses = [
    "192.168.3.200"
  ]
  ttl = 600
}
