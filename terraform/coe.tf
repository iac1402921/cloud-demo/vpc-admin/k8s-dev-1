data "openstack_containerinfra_clustertemplate_v1" "clustertemplate-1-26-7" {
  name = "k8s-1.26.7"
}

#-----------
## Create Cluster k8s-dev-1.26.7
#-----------

resource "openstack_containerinfra_cluster_v1" "k8s-dev-1-26-7" {
  name                = "k8s-dev-1.26.7"
  cluster_template_id = data.openstack_containerinfra_clustertemplate_v1.clustertemplate-1-26-7.id
  master_count        = 3
  node_count          = 2
  keypair             = openstack_compute_keypair_v2.terraform.name
  create_timeout      = 30
}
#
#resource "openstack_containerinfra_nodegroup_v1" "k8s-dev-1-26-7-nodegroup" {
#  name                = "worker"
#  cluster_id          = openstack_containerinfra_cluster_v1.k8s-dev-1-26-7.id
#  node_count          = 1
#  min_node_count      = 1
#  max_node_count      = 2
#  role                = "worker"
#}